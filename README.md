#Social Energy web services

After registering in GSRN, the user completes a questionnaire that assists the process of identifying educational goals, and missing competencies. Based on the questionnaire results an individual learning plan is created in LCMS of behalf of the GSRN covering the missing competencies.
LCMS expose user profile to GSRN in a structured type as described:

## User profile general structure
````
list of ( 
        object {
                user object {
                        id int   //ID of the user
                        username string  Optional //The username
                        firstname string  Optional //The first name(s) of the user
                        lastname string  Optional //The family name of the user
                        email string  Optional //An email address - allow email as root@localhost
                        url string   //Profile URL.
                        firstaccess int  Optional //first access to the site (0 if never)
                        lastaccess int  Optional //last access to the site (0 if never)
                } 
                competencies list of ( 
                        object {
                                id int  Optional //Competence id
                                name string   //Competence name
                                description string   //Competence description
                                idnumber string   //id number
                                proficiency int  Default to "0" //proficiency
                                grade int   //grade type
                                gradename string   //grade name
                        } 
                )
                badges list of ( 
                        object {
                                id int  Optional //Badge id.
                                name string   //Badge name.
                                description string   //Badge description.
                                url string   //Badge URL.
                                dateissued int   //Date issued.
                        } 
                )
                courses list of ( 
                        object {
                                id int   //id of course
                                name string   //long name of course
                                description string  Optional //summary
                                url string   //Course URL.
                                grademin string  Optional //Grade min value
                                grademax string  Optional //Grade max value
                                gradepass string  Optional //Grade pass value
                                grade string  Optional //Grade value
                                dategraded int  Optional //Date issued.
                                progress double  Optional //Progress percentage
                                timespent string  Optional //Dedication time of the user to the course
                        } 
                )
        } 
)
````

#ACKNOWLEDGMENT

Funding for this work was provided by the EC H2020 project SOCIALENERGY [http://socialenergy-project.eu/](http://socialenergy-project.eu/) Grant  agreement  No.  731767.